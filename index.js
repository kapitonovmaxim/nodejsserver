const express = require('express')
const path = require('path')
// const keys = require('./keys')

const app = express()


//connect static files
app.use(express.static(path.join(__dirname, 'public')))

app.use(express.urlencoded({ extended: true }))


const PORT = process.env.PORT || 3000 || 3001

async function start() {
  try {
    app.listen(PORT, () => {
      console.log(`Сервер ожидает на порту: ${PORT}`);
    });
  } catch (e) {
    console.log(e);
  }
}

start();